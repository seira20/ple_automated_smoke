﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using PLE_Automated_Smoke.Util;
using OpenQA.Selenium.Support.UI;

namespace PLE_Automated_Smoke.Validation
{
    public class ValidationClass
    {
        Configuration util = new Configuration();

        public void validateStringIsCorrect(IWebDriver driver, string Expected, string Actual)
        {
            Assert.AreEqual(Expected, Actual);
        }

        public void validateStringInstance(IWebDriver driver, String Expected_String, String String_Instance)
        {
            Assert.IsTrue(Expected_String.Contains(String_Instance));
        }

        public void validateElementIsPresent(IWebDriver driver, By element)
        {
            Assert.IsTrue(util.CheckElement(driver, element, 5));
        }

        public void validateElementIsNotEnabled(IWebDriver driver, By element)
        {
            Assert.IsFalse(driver.FindElement(element).Enabled);
        }

        public void validateElementIsEnabled(IWebDriver driver, By element)
        {
            Assert.IsTrue(driver.FindElement(element).Enabled);
        }

        public void validateCountIsGreaterThanTheExpected(IWebDriver driver, int Exp_Count, By element)
        {
            Assert.Greater(driver.FindElements(element).Count, Exp_Count);
        }
    }
}
