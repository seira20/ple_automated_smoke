﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System.Configuration;
using System;
using System.Net;
using System.Collections.Specialized;
using System.Net.Http;
using System.Linq;
using System.Data.SqlClient;
using System.Data;

namespace PLE_Automated_Smoke.Util
{
    public class Configuration
    {
        public static DataTable dtElements;

        private static IWebDriver driver;

        public static string Browser;

        public static string Environment;

        public static string Env_Url;

        public static string Sel_Host;

        public IWebDriver SeleniumSetup()
        {
            NameValueCollection node = (NameValueCollection)ConfigurationManager.GetSection("GridConfig");
            Environment = node["environment"];
            Browser = node["browser"];
            Sel_Host = node["sel_host"];

            if (Environment.Equals("production"))
            {
                Env_Url = "https://ple.web.analog.com/";
            }
            else if (Environment.Equals("qa"))
            {
                Env_Url = "https://ple-qa.web.analog.com/";
            }
           
            switch (Browser)
            {
                case "Chrome":
                    ChromeOptions options = new ChromeOptions();
                    options.AddArguments("start-maximized");
                    options.AddArguments("--ignore-certificate-errors");
                    options.AddArguments("--disable-extensions");
                    //options.AddArguments("--auto-open-devtools-for-tabs");
                    driver = new RemoteWebDriver(new Uri(Sel_Host), options);
                    return driver;

                case "Chrome_HL":
                    ChromeOptions options2 = new ChromeOptions();
                    options2.AddArguments("--headless");
                    options2.AddArguments("--disable-gpu");
                    options2.AddArguments("--disable-extensions");
                    options2.AddArguments("--ignore-certificate-errors");
                    options2.AddArguments("window-size=1280,1696");
                    driver = new RemoteWebDriver(new Uri(Sel_Host), options2);
                    return driver;

                    //case "Headless":
                    //    capabilities = DesiredCapabilities.HtmlUnit();
                    //    capabilities.SetCapability(CapabilityType.BrowserVersion, "CHROME");
                    //    capabilities.SetCapability(CapabilityType.IsJavaScriptEnabled, true);
                    //    driver = new RemoteWebDriver(new Uri(Sel_Host), capabilities);
                    //    return driver;

            }

            return null;


        }
        public void GetWebRequest(string Url)
        {

            using (var client = new HttpClient())
            {
                var responseString = client.GetStringAsync(Url);
            }

            ////string html = string.Empty;
            //string url = @Url;

            //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            ////request.AutomaticDecompression = DecompressionMethods.GZip;

            ////using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            ////using (Stream stream = response.GetResponseStream())
            ////using (StreamReader reader = new StreamReader(stream))
            ////{
            ////    html = reader.ReadToEnd();
            ////}

            ////Console.WriteLine(html);
        }
        public int GetWebSiteReponse(String Url)
        {
            int statusCode = 0;
            HttpWebResponse response = null;
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Ssl3 | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls;
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(Url);
                webRequest.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
                webRequest.UserAgent = "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36";
                webRequest.Method = "GET";
                webRequest.AllowAutoRedirect = false;
                response = (HttpWebResponse)webRequest.GetResponse();

                statusCode = (int)response.StatusCode;

                response.Close();

                return statusCode;
            }
            catch (WebException wex)
            {
                response = (HttpWebResponse)wex.Response;
                statusCode = (int)response.StatusCode;

                response.Close();

                return statusCode;
            }
        }
        public bool CheckElement(IWebDriver driver, By element, int timeout)
        {
            try
            {
                //var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
                var myElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(element));
                return myElement.Displayed;
            }
            catch
            {
                return false;
            }
        }
        public bool IsAscendingOrder(object[] value)
        {
            var orderedByAsc = value.OrderBy(d => d);

            bool sort = false;

            if (value.SequenceEqual(orderedByAsc))
            {
                sort = true;
            }

            return sort;
        }
        public string GetText(IWebDriver driver, By element)
        {

            string Element_text = driver.FindElement(element).Text;

            return Element_text;

        }
        public string ReturnAttribute(IWebDriver driver, By element, string AttributeToReturn)
        {

            string attributeValue = driver.FindElement(element).GetAttribute(AttributeToReturn);
            return attributeValue;

        }
        public string GetCssValue(IWebDriver driver, By element, string attribute)
        {
            string cssValue = null;

            if (CheckElement(driver, element, 5))
            {
                cssValue = driver.FindElement(element).GetCssValue(attribute);
            }

            return cssValue;
        }
        public string Generate_Project_Name()
        {
            DateTime EstDateTime = DateTime.UtcNow;
            EstDateTime = TimeZoneInfo.ConvertTimeFromUtc(EstDateTime, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));

            const string number = "1234567890";
            var random3 = new Random();
            string rand3 = new string(Enumerable.Repeat(number, 2)
              .Select(s => s[random3.Next(s.Length)]).ToArray());

            string ProjName = "PLE_E2E_Test_" + EstDateTime.ToString("Mdyyyy") + "_" + rand3;

            return ProjName;
        }
        public string GetCredentials(string columnName, string columnName2)
        {

            string cn = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection myConnection = new SqlConnection(cn);
            myConnection.Open();
            SqlCommand myCommand = new SqlCommand("SELECT * FROM tbl_UserAccount", myConnection);

            dtElements = new DataTable();
            dtElements.Load(myCommand.ExecuteReader());

            myConnection.Close();

            return dtElements.Select(string.Format("Role = '{0}'", columnName))[0][columnName2].ToString();

        }
        public string GetAlertMessageThenAccept(IWebDriver driver)
        {
            string AlertMsg = "";

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20) /*timeout in seconds*/);
            if (wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.AlertIsPresent()) == null)
            {
                return AlertMsg = "No Alert Message";
            }
            else
            {
                try
                {
                    AlertMsg = driver.SwitchTo().Alert().Text;
                    driver.SwitchTo().Alert().Accept();
                    return AlertMsg;
                }
                catch (NoAlertPresentException)
                {
                    return AlertMsg = "No Alert Message";
                }
            }

        }
        public string RandomNumber()
        {
            DateTime EstDateTime = DateTime.UtcNow;
            EstDateTime = TimeZoneInfo.ConvertTimeFromUtc(EstDateTime, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));

            const string number = "1234567890";
            var random3 = new Random();
            string rand3 = new string(Enumerable.Repeat(number, 2)
              .Select(s => s[random3.Next(s.Length)]).ToArray());

           return rand3;

        }

        public int GetElementCount(IWebDriver driver, By element) {

            int Count = driver.FindElements(element).Count;

            return Count;

        }
    }
}
