﻿using NUnit.Framework;
using OpenQA.Selenium;
using PLE_Automated_Smoke.Util;
using PLE_Automated_Smoke.Action;
using PLE_Automated_Smoke.Validation;
using System;

namespace PLE_Automated_Smoke.Util
{
    [Parallelizable]
    [TestFixture]
    public class BaseSetUp
    {
        protected IWebDriver driver;
        protected Configuration util;
        protected Actionkey action;
        protected ValidationClass test;

        public static bool _stopTests;

        public BaseSetUp()
        {
            this.util = new Configuration();
            this.action = new Actionkey();
            this.test = new ValidationClass();
        }


        [SetUp]
        public void Init()
        {
            Assume.That(_stopTests, Is.False);
            driver = util.SeleniumSetup();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }

        [TearDown]
        public void Cleanup()
        {
            if (TestContext.CurrentContext.Result.Outcome.Status != NUnit.Framework.Interfaces.TestStatus.Passed)
            {
                _stopTests = true;
                driver.Quit();
            }
            else
            { 
                driver.Quit();
            }
        }
    }
}
