﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using PLE_Automated_Smoke.Util;
using System;
using System.Threading;

namespace PLE_Automated_Smoke.Action
{
    public class Actionkey
    {
        Configuration util = new Configuration();

        public void Navigate(IWebDriver driver, string url)
        {
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(url);
        }

        public void IClick(IWebDriver driver, By element)
        {
            if (util.CheckElement(driver, element, 5))
            {
                driver.FindElement(element).Click();
                Thread.Sleep(2000);
            }
        }

        public void IType(IWebDriver driver, By element, string keyword)
        {
            if (util.CheckElement(driver, element, 5))
            {
                driver.FindElement(element).SendKeys(keyword);
                Thread.Sleep(2000);
            }
        }

        public void IMouseHove(IWebDriver driver, By element) {
            Actions Hover = new Actions(driver);
            Hover.MoveToElement(driver.FindElement(element)).Perform();
            Thread.Sleep(2000);
        }

        public void IClear(IWebDriver driver, By element) {
            if (util.CheckElement(driver, element, 5))
            {
                driver.FindElement(element).Clear();
                Thread.Sleep(2000);
                //--- Added this to make sure that the values are deleted incase the Clear() function didn't work ---//
                if (!driver.FindElement(element).GetAttribute("value").Equals(""))
                {
                    for (int ctr = util.ReturnAttribute(driver, element, "value").Length; ctr != 0; ctr--)
                    {
                        driver.FindElement(element).SendKeys(Keys.Backspace);
                        Thread.Sleep(1000);
                    }
                }
            }
        }

        public void SubmitStandardProj(IWebDriver driver, string ProjectName)
        {
            IType(driver, By.CssSelector("input[id='projectName']"), ProjectName);
            //******Dev Track********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlDevelopmentTrack_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlDevelopmentTrack_listbox']>li[data-offset-index='1']"));
            //*****Project Description****/
            IType(driver, By.CssSelector("input[id='projectDesc']"), "Test Project E2E");
            //****Application Problem*****/
            IType(driver, By.CssSelector("textarea[id='projectDetails']"), "Test_Application Problem");
            //******Buss Unit********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlBusinessUnit_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlBusinessUnit_listbox']>li[data-offset-index='8']"));
            //*****Strategy********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlStrategy_listbox']"));
            IClick(driver, By.CssSelector("ul[id='ddlStrategy_listbox']>li[data-offset-index='0']"));
            //******Sub Strat Unit********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlSubStrategy_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlSubStrategy_listbox']>li[data-offset-index='0']"));
            //******Portfolio********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlPortfolio_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlPortfolio_listbox']>li[data-offset-index='0']"));
            //******Classification********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlClassificationType_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlClassificationType_listbox']>li[data-offset-index='0']"));
            //*****App Type********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlApplicationType_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlApplicationType_listbox']>li[data-offset-index='0']"));
            //******Functional Safety********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlFunctionalSafety_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlFunctionalSafety_listbox']>li[data-offset-index='1']"));
            /******Proj Champ*****/
            IType(driver, By.CssSelector("div[id='championpeoplePicker_TopSpan']>input[class='sp-peoplepicker-editorInput']  "), "Sorosoro Aries");
            IClick(driver, By.CssSelector("div[id='championpeoplePicker_TopSpan_AutoFillDiv']>ul>li>a>div"));
            /******Security********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlSecurity_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlSecurity_listbox']>li[data-offset-index='2']"));
            /******Market Type********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlMarketType_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlMarketType_listbox']>li[data-offset-index='0']"));
            /******horizon Type********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlHorizon_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlHorizon_listbox']>li[data-offset-index='0']"));
            IClick(driver, By.CssSelector("div[id='divSave']>button[data-bind*='click: submitaction']"));
            Thread.Sleep(3000);
        }

        public void SubmitSystemProj(IWebDriver driver, string ProjectName)
        {
            IType(driver, By.CssSelector("input[id='projectName']"), ProjectName);
            //******Dev Track********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlDevelopmentTrack_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlDevelopmentTrack_listbox']>li[data-offset-index='3']"));
            //*****Project Description****/
            IType(driver, By.CssSelector("input[id='projectDesc']"), "Test Project E2E");
            //****Application Problem*****/
            IType(driver, By.CssSelector("textarea[id='projectDetails']"), "Test_Application Problem");
            //******Buss Unit********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlBusinessUnit_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlBusinessUnit_listbox']>li[data-offset-index='8']"));
            //*****Strategy********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlStrategy_listbox']"));
            IClick(driver, By.CssSelector("ul[id='ddlStrategy_listbox']>li[data-offset-index='0']"));
            //******Sub Strat Unit********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlSubStrategy_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlSubStrategy_listbox']>li[data-offset-index='0']"));
            //******Portfolio********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlPortfolio_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlPortfolio_listbox']>li[data-offset-index='0']"));
            //******Classification********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlClassificationType_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlClassificationType_listbox']>li[data-offset-index='0']"));
            //*****App Type********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlApplicationType_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlApplicationType_listbox']>li[data-offset-index='0']"));
            //******Functional Safety********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlFunctionalSafety_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlFunctionalSafety_listbox']>li[data-offset-index='1']"));
            /******Proj Champ*****/
            IType(driver, By.CssSelector("div[id='championpeoplePicker_TopSpan']>input[class='sp-peoplepicker-editorInput']  "), "Sorosoro Aries");
            IClick(driver, By.CssSelector("div[id='championpeoplePicker_TopSpan_AutoFillDiv']>ul>li>a>div"));
            /******Security********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlSecurity_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlSecurity_listbox']>li[data-offset-index='2']"));
            /******Market Type********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlMarketType_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlMarketType_listbox']>li[data-offset-index='0']"));
            /******horizon Type********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlHorizon_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlHorizon_listbox']>li[data-offset-index='0']"));
            IClick(driver, By.CssSelector("div[id='divSave']>button[data-bind*='click: submitaction']"));
            Thread.Sleep(3000);
        }

        public void SubmitModuleProj_Streamline(IWebDriver driver, string ProjectName) {
            IType(driver, By.CssSelector("input[id='projectName']"), ProjectName);
            //******Dev Track********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlDevelopmentTrack_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlDevelopmentTrack_listbox']>li[data-offset-index='2']"));
            //*****Project Description****/
            IType(driver, By.CssSelector("input[id='projectDesc']"), "Test Project E2E");
            //****Application Problem*****/
            IType(driver, By.CssSelector("textarea[id='projectDetails']"), "Test_Application Problem");
            //******Buss Unit********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlBusinessUnit_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlBusinessUnit_listbox']>li[data-offset-index='8']"));
            //*****Strategy********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlStrategy_listbox']"));
            IClick(driver, By.CssSelector("ul[id='ddlStrategy_listbox']>li[data-offset-index='0']"));
            //******Sub Strat Unit********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlSubStrategy_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlSubStrategy_listbox']>li[data-offset-index='0']"));
            //******Portfolio********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlPortfolio_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlPortfolio_listbox']>li[data-offset-index='0']"));
            //******Classification********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlClassificationType_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlClassificationType_listbox']>li[data-offset-index='0']"));
            //*****App Type********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlApplicationType_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlApplicationType_listbox']>li[data-offset-index='0']"));
            //******Functional Safety********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlFunctionalSafety_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlFunctionalSafety_listbox']>li[data-offset-index='2']"));
            /*******Dev Flow******/
            IClick(driver, By.CssSelector("label[for='rdStreamlined']"));
            /******Proj Champ*****/
            IType(driver, By.CssSelector("div[id='championpeoplePicker_TopSpan']>input[class='sp-peoplepicker-editorInput']  "), "Sorosoro Aries");
            IClick(driver, By.CssSelector("div[id='championpeoplePicker_TopSpan_AutoFillDiv']>ul>li>a>div"));
            /******Security********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlSecurity_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlSecurity_listbox']>li[data-offset-index='2']"));
            /******Market Type********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlMarketType_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlMarketType_listbox']>li[data-offset-index='0']"));
            /******horizon Type********/
            IClick(driver, By.CssSelector("span[aria-owns='ddlHorizon_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlHorizon_listbox']>li[data-offset-index='0']"));
            IClick(driver, By.CssSelector("div[id='divSave']>button[data-bind*='click: submitaction']"));
            Thread.Sleep(3000);
        }

        public void UserSearchForProjectRequestViaSearchTextbox(IWebDriver driver, string ProjName)
        {  
            for (int x =1 ; x<10 ; x++ ) {
                IType(driver, By.CssSelector("input[id='searchText']"), ProjName);
                Thread.Sleep(3000);
                if (util.CheckElement(driver, By.CssSelector("div[class='autocomplete-suggestion']>b"), 5)) {
                    break;
                }
                driver.Navigate().Refresh();
                Thread.Sleep(10000);
            }
            IClick(driver, By.CssSelector("div[class='autocomplete-suggestion']>b"));
            Thread.Sleep(10000);
        }

        public void Search_AndApproveNewProjectRequest(IWebDriver driver, string ProjName)
        {
            IClick(driver, By.CssSelector("ul[class='k-tabstrip-items k-reset']>li:nth-child(2)>span[class='k-link']"));
            if(util.CheckElement(driver, By.CssSelector("span[id='createdBy_TopSpan_ResolvedList'] a[class='sp-peoplepicker-delImage']"),5))
            {
                IClick(driver, By.CssSelector("span[id='createdBy_TopSpan_ResolvedList'] a[class='sp-peoplepicker-delImage']"));
            }
            IType(driver, By.CssSelector("input[aria-owns='prjNameReq_taglist prjNameReq_listbox']"), ProjName);
            IClick(driver, By.CssSelector("ul[id='prjNameReq_listbox']>li"));
            IClick(driver, By.CssSelector("button[data-bind*='searchPrj']"));
            IClick(driver, By.CssSelector("div[id='prjReqGrid']>div[class='k-grid-content k-auto-scrollable']>table>tbody>tr>td>a"));
            Thread.Sleep(10000);
            IClick(driver, By.CssSelector("div[id='divSave']>button[data-bind*='approve']"));
        }

        public void AddProjectTechnicalLead(IWebDriver driver, string User1, string User2) {
            //add Proj Lead/Manager
            IClick(driver, By.CssSelector("div[id='teamsDivForProdDev'] span[title='Click to Add a new item']"));
            IClick(driver, By.CssSelector("span[aria-owns='ddlMemberRole1_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlMemberRole1_listbox']>li[data-offset-index='26']"));
            IType(driver, By.CssSelector("div[id='newptgPicker_TopSpan']>input[id='newptgPicker_TopSpan_EditorInput']"), User1);
            IClick(driver, By.CssSelector("div[id='newptgPicker_TopSpan_AutoFillDiv']>ul>li>a>div"));
            IClick(driver, By.CssSelector("button[data-bind='click: addTeamListItem, visible: isSaveVisible']"));
            Thread.Sleep(5000);
            //Add Technical Lead
            IClick(driver, By.CssSelector("div[id='teamsDivForProdDev'] span[title='Click to Add a new item']"));
            IClick(driver, By.CssSelector("span[aria-owns='ddlMemberRole1_listbox']>span"));
            IClick(driver, By.CssSelector("ul[id='ddlMemberRole1_listbox']>li[data-offset-index='39']"));
            IType(driver, By.CssSelector("div[id='newptgPicker_TopSpan']>input[id='newptgPicker_TopSpan_EditorInput']"), User2);
            IClick(driver, By.CssSelector("div[id='newptgPicker_TopSpan_AutoFillDiv']>ul>li>a>div"));
            IClick(driver, By.CssSelector("button[data-bind='click: addTeamListItem, visible: isSaveVisible']"));
            Thread.Sleep(5000);
        }

        public void SetSecurityExposureToZero(IWebDriver driver)
        {
            if (util.CheckElement(driver, By.CssSelector("div[id='divEditProject']"), 5))
            {
                IClick(driver, By.CssSelector("span[title='Click to switch to VIEW MODE']"));
                Thread.Sleep(1500);
                driver.SwitchTo().Alert().Dismiss();
                Thread.Sleep(1000);
            }
            IClick(driver, By.CssSelector("span[title='Click to switch to EDIT MODE']"));
            for (int x = 1; util.CheckElement(driver, By.CssSelector("div[data-template='pvdpsaEdittemplate']>div:nth-child("+ x +")"),5); x++) {
                IClick(driver, By.CssSelector("div[data-template='pvdpsaEdittemplate']>div:nth-child(" + x + ") label[for*='No']"));
            }
            IType(driver, By.CssSelector("textarea[id='psacomment']"), "Test Comment");
            IClick(driver, By.CssSelector("span[title='Click to Save to Database']"));
            Thread.Sleep(5000);
            driver.SwitchTo().Alert().Accept();
            Thread.Sleep(5000);
            driver.SwitchTo().Alert().Accept();
            Thread.Sleep(5000);
        }

        public void EditInvestmentBC(IWebDriver driver, string RandomNumber)
        {
            if (util.CheckElement(driver, By.CssSelector("div[id='divEditProject']"), 5))
            {
                IClick(driver, By.CssSelector("span[title='Click to switch to VIEW MODE']"));
                Thread.Sleep(1500);
                driver.SwitchTo().Alert().Dismiss();
                Thread.Sleep(1000);
            }
            IClick(driver, By.CssSelector("span[title='Click to switch to EDIT MODE']"));
            IClear(driver, By.CssSelector("input[id='labor']"));
            IType(driver, By.CssSelector("input[id='labor']"), RandomNumber);
            IClick(driver, By.CssSelector("span[title='Click to Save to Database']"));
            Thread.Sleep(5000);
            driver.SwitchTo().Alert().Accept();
            Thread.Sleep(5000);
            driver.SwitchTo().Alert().Accept();
            Thread.Sleep(5000);
        }

        public void CreateGenerics(IWebDriver driver, string DevFlow, string GenericName)
        {
            IClick(driver, By.CssSelector("button[id='createNewGen']"));
            IType(driver, By.CssSelector("input[id='genericName']"), GenericName);
            //Select Generic Class
            IClick(driver, By.CssSelector("span[aria-owns='prdClassification_listbox']"));
            IClick(driver, By.CssSelector("ul[id='prdClassification_listbox']>li:nth-child(1)"));
            //Select Gtm Release Type
            IClick(driver, By.CssSelector("span[aria-owns='gtmReleaseType_listbox']"));
            IClick(driver, By.CssSelector("ul[id='gtmReleaseType_listbox']>li:nth-child(1)"));
            if (DevFlow.Equals("standard"))
            { 
                //Select FS
                IClick(driver, By.CssSelector("span[aria-owns='ddlFS_listbox']"));
                IClick(driver, By.CssSelector("ul[id='ddlFS_listbox']>li:nth-child(2)"));
            }
            
            //Select Security Level
            IClick(driver, By.CssSelector("span[aria-owns='ddlSec_listbox']"));
            IClick(driver, By.CssSelector("ul[id='ddlSec_listbox']>li:nth-child(2)"));
            //Select RTS Date
            IType(driver, By.CssSelector("input[data-bind='value: releasetoSales']"), TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")).ToString("dd-MMM-yyyy"));
            //Click Submit button
            IClick(driver, By.CssSelector("button[data-bind='click: savetodatabase']"));
            Thread.Sleep(3000);
        }

        public void DownloadAndSaveChecklist(IWebDriver driver)
        {
            IClick(driver, By.CssSelector("div[id='panelBarSection'] span[title='Download Current Phase Template']"));
            Thread.Sleep(5000);

            IClick(driver, By.CssSelector("div[class*='k-widget k-window'] div[id='initiatePhasePopup'] button[title='Click to initiate phase with appropriate template.']"));
            IClick(driver, By.CssSelector("div[class*='k-widget k-window'] div[id='initiatePhasePopup'] button[title='Download current phase template']"));
            Thread.Sleep(5000);
            IClick(driver, By.CssSelector("button[data-bind*='click: saveCheckList']"));
        }

        public void SignOff_Task(IWebDriver driver, string Title, string Url) {

                IType(driver, By.CssSelector("div[class*='k-widget k-window'] div[id='taskSignOffPopup'] input[id='linkAttachmentTitle']"), Title );
                IType(driver, By.CssSelector("div[class*='k-widget k-window'] div[id='taskSignOffPopup'] input[id='linkAttachmentUrl']"), Url);
                IClick(driver, By.CssSelector("div[class*='k-widget k-window'] div[id='taskSignOffPopup'] button[data-bind*='addLinkAttachmentVM']"));
                IClick(driver, By.CssSelector("div[class*='k-widget k-window'] button[id='signOffPopupSaveBtn']"));
        }

        public void ProcessGtM(IWebDriver driver, string Approver) {
            IClick(driver, By.CssSelector("input[id='CheckedTasks']"));
            if (driver.FindElement(By.CssSelector("button[title='Save GtM Task Modifications']")).Enabled)
            { 
                for (int y = 1; util.CheckElement(driver, By.CssSelector("div[id='panelBarSection'] table[id='tbl']>tbody>tr:nth-child(" + y + ")"), 5); y++)
                {
                    if (driver.FindElement(By.CssSelector("div[id='panelBarSection'] table[id='tbl']>tbody>tr:nth-child(" + y + ")>td>input")).Selected)
                    {
                        if (!util.CheckElement(driver, By.CssSelector("div[id='panelBarSection'] table[id='tbl']>tbody>tr:nth-child(" + y + ")>td:nth-child(4) * div[class='sp-peoplepicker-topLevel'] a[class='sp-peoplepicker-delImage']"), 5).Equals(true))
                        {
                            IType(driver, By.CssSelector("div[id='panelBarSection'] table[id='tbl']>tbody>tr:nth-child(" + y + ")>td:nth-child(4) * div[class='sp-peoplepicker-topLevel']>input[class='sp-peoplepicker-editorInput']"), Approver);
                        }
                            IClick(driver, By.CssSelector("div[id='panelBarSection'] table[id='tbl']>tbody>tr:nth-child(" + y + ")>td:nth-child(4) * div[id='gtmPicker" + y + "_TopSpan_AutoFillDiv']>ul>li"));
                            IClick(driver, By.CssSelector("div[id='panelBarSection'] table[id='tbl']>tbody>tr:nth-child(" + y + ")>td:nth-child(5)>span[aria-owns='ddlGTMStatus_listbox']>span"));
                            IClick(driver, By.CssSelector("div[class='k-list-container k-popup k-group k-reset k-state-border-up'] ul[id = 'ddlGTMStatus_listbox']>li:nth-child(2)"));
                                                
                    }
                }
                IClick(driver, By.CssSelector("button[title='Save GtM Task Modifications']"));
                Thread.Sleep(5000);
                driver.SwitchTo().Alert().Accept();
                Thread.Sleep(5000);
            }
            IClick(driver, By.CssSelector("button[data-bind='click: onCloseGtMPopUp']"));
        }

        public void Initiate_PhaseGate(IWebDriver driver, string Input_String)
        {
            IClick(driver, By.CssSelector("button[id='btn_initiateWF']"));
            IType(driver, By.CssSelector("textarea[data-bind='value:milestonesComment']"), Input_String);
            IClick(driver, By.CssSelector("button[id='btn_initiateWFPopup']"));
        }

        public void Group_Generic(IWebDriver driver)
        {
            IClick(driver, By.CssSelector("button[id='manageGroupGen']"));
            IClick(driver, By.CssSelector("div[id='managegroups1'] div[data-bind*='UnGroupRowVisible'] button[data-bind*='click: createEditGroups']"));

            for (int x = 1; util.CheckElement(driver, By.CssSelector("div[id='manageCreateEditDiv'] div[data-bind*='unGroupedRowVisible'] div[id='genericgrid'] * tbody>tr:nth-child(" + x + ")"), 5); x++)
            {
                IClick(driver, By.CssSelector("div[id='manageCreateEditDiv'] div[data-bind*='unGroupedRowVisible'] div[id='genericgrid'] * tbody>tr:nth-child(" + x + ") * input[id='chkBxIdE']"));
            }

            IClick(driver, By.CssSelector("label[for='rdMileStone']"));
            IClick(driver, By.CssSelector("button[data-bind='click: nextManagePopup']"));
            util.GetAlertMessageThenAccept(driver);
            IClick(driver, By.CssSelector("button[data-bind*='click: SaveGeneric']"));
            Thread.Sleep(15000);
        }

        public void UnGroup_Generic(IWebDriver driver)
        {
            IClick(driver, By.CssSelector("button[id='manageGroupGen']"));
            for (int x = 1; util.CheckElement(driver, By.CssSelector("div[id='managegroups'] div[data-bind='visible: GroupARowVisible'] div[id='genericgrid'] * tbody>tr:nth-child(" + x + ")"), 5); x++)
            {
                IClick(driver, By.CssSelector("div[id='managegroups'] div[data-bind='visible: GroupARowVisible'] div[id='genericgrid'] * tbody>tr:nth-child(" + x + ") * input[id='chkBxIdA']"));
            }

            IClick(driver, By.CssSelector("button[data-bind='click: removeGroupsA']"));
            util.GetAlertMessageThenAccept(driver);
            Thread.Sleep(15000);
        }

        public void Add_PSD2x(IWebDriver driver)
        {
            IClick(driver, By.CssSelector("span[title='Click to create PSD 2.X Iteration.']"));
            //IClick(driver, By.CssSelector("div[id='initiateIterationPhasePopup'] span[aria-owns='Select1_listbox']"));
            //for (int x = 1; x <= 5; x++) {
            //    if (util.GetText(driver, By.CssSelector("ul[id='Select1_listbox']>li:nth-child(" + x + ")")).Equals("AIS - ADI - QA Base Template - o6082018")) {
            //        IClick(driver, By.CssSelector("ul[id='Select1_listbox']>li:nth-child(" + x + ")"));
            //        break;
            //    }
            //}
            IClick(driver, By.CssSelector("div[id='initiateIterationPhasePopup'] * button"));
        }

    }

}


