﻿using PLE_Automated_Smoke.Util;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System.Linq;
using System;

namespace PLE_Automated_Smoke.SiPTrack
{

    [TestFixture]
    public class SiP_MultipleGroupedGenerics : BaseSetUp
    {
        public SiP_MultipleGroupedGenerics() : base() { }

        string ProjName = null;
        string ProjUrl = null;
        string GenericName1 = null;
        string GenericName2 = null;
        string DateToday = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")).ToString("dd-MMM-yyyy");

        [Test, Order(1)]
        [TestCase(TestName = "Test 1 - Pre-requisite: Creating a project then proceeding in completing PSD1 in 1st created generic")]
        public void Create_Standard_Project_With_Multiple_Generics()
        {
            ProjName = util.Generate_Project_Name();

            action.Navigate(driver, Configuration.Env_Url.Replace("https://", "https://" + util.GetCredentials("Account 1", "Username") + ":" + util.GetCredentials("Account 1", "Password") + "@") + "SiteAssets/Pages/ProjectRequest.aspx");
            Thread.Sleep(15000);
            action.SubmitStandardProj(driver, ProjName);
            util.GetAlertMessageThenAccept(driver);
            action.UserSearchForProjectRequestViaSearchTextbox(driver, ProjName);
            ProjUrl = driver.Url;
            GenericName1 = DateToday + "_" + util.RandomNumber();
            action.CreateGenerics(driver, "standard", GenericName1);
            util.GetAlertMessageThenAccept(driver);
            driver.Navigate().Refresh();
            Thread.Sleep(15000);
            action.IClick(driver, By.CssSelector("div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable'] * tbody>tr * a[class='k-icon k-i-expand']"));
            action.IClick(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(1)>span"));
            action.DownloadAndSaveChecklist(driver);
            int Taskcount = util.GetElementCount(driver, By.CssSelector("table[id='psd100Grid']>tbody>tr"));
            for (int x = 1; util.CheckElement(driver, By.CssSelector("table[id='psd100Grid']>tbody>tr:nth-child(" + x + ")"), 5); x++)
            {
                if (util.CheckElement(driver, By.CssSelector("table[id='psd100Grid']>tbody>tr:nth-child(" + x + ") * span[class='ptc_pending']"), 5))
                {
                    if (util.CheckElement(driver, By.CssSelector("table[id='psd100Grid']>tbody>tr:nth-child(" + x + ")>td>button"), 5))
                    {
                        action.IClick(driver, By.CssSelector("table[id='psd100Grid']>tbody>tr:nth-child(" + x + ")>td>button"));
                        Thread.Sleep(2000);
                        action.ProcessGtM(driver, "Sorosoro, Aries");
                    }

                    action.IClick(driver, By.CssSelector("table[id='psd100Grid']>tbody>tr:nth-child(" + x + ") * span[class='ptc_pending']"));
                    action.SignOff_Task(driver, "Test Only", "http://www.analog.com");
                    if (x == Taskcount)
                    {
                        driver.Navigate().Refresh();
                        Thread.Sleep(15000);
                        action.IClick(driver, By.CssSelector("div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable'] * tbody>tr * a[class='k-icon k-i-expand']"));
                        action.IClick(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(1)>span"));
                    }
                }
                Thread.Sleep(3000);
            }
        }

        [Test, Order(2)]
        [TestCase(TestName = "Test 2 - Verify User can create multiple generics")]
        public void Create_Multiple_Generic()
        {
            GenericName2 = DateToday + "_" + util.RandomNumber();
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            action.CreateGenerics(driver, "standard", GenericName2);
            util.GetAlertMessageThenAccept(driver);
            test.validateCountIsGreaterThanTheExpected(driver, 1, By.CssSelector("div[id='genericInfoDiv'] div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable']>table>tbody>tr"));
        }

        [Test, Order(3)]
        [TestCase(TestName = "Test 3 - Verify that user can group generics")]
        public void Group_Generics()
        {
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            action.IClick(driver, By.CssSelector("button[id='manageGroupGen']"));
            action.IClick(driver, By.CssSelector("div[id='managegroups1'] div[data-bind*='UnGroupRowVisible'] button[data-bind*='click: createEditGroups']"));
            string Groupname = util.GetText(driver, By.CssSelector("span[aria-owns='GroupDrpdwn_listbox']>span>span"));
            for (int x = 1; util.CheckElement(driver, By.CssSelector("div[id='manageCreateEditDiv'] div[data-bind*='unGroupedRowVisible'] div[id='genericgrid'] * tbody>tr:nth-child(" + x + ")"), 5); x++)
            {
                action.IClick(driver, By.CssSelector("div[id='manageCreateEditDiv'] div[data-bind*='unGroupedRowVisible'] div[id='genericgrid'] * tbody>tr:nth-child(" + x + ") * input[id='chkBxIdE']"));
            }

            action.IClick(driver, By.CssSelector("label[for='rdMileStone']"));
            action.IClick(driver, By.CssSelector("button[data-bind='click: nextManagePopup']"));
            test.validateStringIsCorrect(driver, "Source will be defaulted to Lead generic ", util.GetAlertMessageThenAccept(driver));
            action.IClick(driver, By.CssSelector("button[data-bind*='click: SaveGeneric']"));
            Thread.Sleep(15000);

            for (int y = 1; util.CheckElement(driver, By.CssSelector("div[id='genericInfoDiv'] div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable']>table>tbody>tr:nth-child(" + y + ")"), 5); y++)
            {
                if (util.GetText(driver, By.CssSelector("div[id='genericInfoDiv'] div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable']>table>tbody>tr:nth-child(" + y + ")>td:nth-child(3)")).Equals("Yes"))
                {
                    test.validateStringIsCorrect(driver, Groupname + " - Source", util.GetText(driver, By.CssSelector("div[id='genericInfoDiv'] div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable']>table>tbody>tr:nth-child(" + y + ")>td:nth-child(11)")));
                }
                else
                {
                    test.validateStringIsCorrect(driver, Groupname, util.GetText(driver, By.CssSelector("div[id='genericInfoDiv'] div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable']>table>tbody>tr:nth-child(" + y + ")>td:nth-child(11)")));
                }
            }
        }

        [Test, Order(4)]
        [TestCase(TestName = "Test 4 - Verify that user can un-group generics")]
        public void UnGroup_Generics()
        {
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            action.IClick(driver, By.CssSelector("button[id='manageGroupGen']"));
            for (int x = 1; util.CheckElement(driver, By.CssSelector("div[id='managegroups'] div[data-bind='visible: GroupARowVisible'] div[id='genericgrid'] * tbody>tr:nth-child(" + x + ")"), 5); x++)
            {
                action.IClick(driver, By.CssSelector("div[id='managegroups'] div[data-bind='visible: GroupARowVisible'] div[id='genericgrid'] * tbody>tr:nth-child(" + x + ") * input[id='chkBxIdA']"));
            }

            action.IClick(driver, By.CssSelector("button[data-bind='click: removeGroupsA']"));
            test.validateStringIsCorrect(driver, "Are you sure you want to delete the selected generic from the group A?", util.GetAlertMessageThenAccept(driver));
            Thread.Sleep(15000);
            for (int y = 1; util.CheckElement(driver, By.CssSelector("div[id='genericInfoDiv'] div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable']>table>tbody>tr:nth-child(" + y + ")"), 5); y++)
            {
                test.validateStringIsCorrect(driver, "N/A", util.GetText(driver, By.CssSelector("div[id='genericInfoDiv'] div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable']>table>tbody>tr:nth-child(" + y + ")>td:nth-child(11)")));
            }

            action.Group_Generic(driver);
        }

        [Test, Order(5)]
        [TestCase(TestName = "Test 5 - Verify that 2nd created generic will be disabled and task status for PSD1 will be based on source")]
        public void Group_Gen_Behavior_Check()
        {
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            action.IClick(driver, By.CssSelector("div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable'] * tbody>tr:nth-child(2) * a[class='k-icon k-i-expand']"));
            test.validateStringIsCorrect(driver, "The PTC section is disabled. Please act on the Source Generic: " + GenericName1, util.GetText(driver, By.CssSelector("tr[class='k-detail-row'] div[id='ptcCheckCompWrapper'] div[data-bind='visible:IsGroupedMsg']>span[data-bind='text:GroupedMsg']")));
            test.validateElementIsPresent(driver, By.CssSelector("tr[class='k-detail-row'] ul[id='panelbarPTC']>li:nth-child(1) span[class='completed_ptc']"));
            //action.UnGroup_Generic(driver);
        }




    }
}