﻿using PLE_Automated_Smoke.Util;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System.Linq;
using System;
using OpenQA.Selenium.Interactions;

namespace PLE_Automated_Smoke.ModuleTrack
{

    [TestFixture]
    public class Mod_SingleGeneric : BaseSetUp
    {
        public Mod_SingleGeneric() : base() { }

        string ProjName = null;
        string ProjUrl = null;
        string GenericName = null;
        string DateToday = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")).ToString("dd-MMM-yyyy");

        [Test, Order(1)]
        [TestCase("Project request has been submitted successfully for approval process.", TestName = "Test 1 - Verify that user can successfully create an Module Project")]
        public void Create_Module_Project(string ExpectedAlert)
        {
            ProjName = util.Generate_Project_Name();

            action.Navigate(driver, Configuration.Env_Url.Replace("https://", "https://" + util.GetCredentials("Account 2", "Username") + ":" + util.GetCredentials("Account 2", "Password") + "@") + "SiteAssets/Pages/ProjectRequest.aspx");
            Thread.Sleep(15000);
            action.SubmitModuleProj_Streamline(driver, ProjName);
            string ActualAlertMessage = util.GetAlertMessageThenAccept(driver);
            test.validateStringIsCorrect(driver, ExpectedAlert, ActualAlertMessage);
            driver.Close();
            driver = util.SeleniumSetup();
            action.Navigate(driver, Configuration.Env_Url.Replace("https://", "https://" + util.GetCredentials("Account 1", "Username") + ":" + util.GetCredentials("Account 1", "Password") + "@") + "SiteAssets/Pages/ProjectHome.aspx");
            Thread.Sleep(15000);
            action.Search_AndApproveNewProjectRequest(driver, ProjName);
            string ActualAlertMessage2 = util.GetAlertMessageThenAccept(driver);
            test.validateStringIsCorrect(driver, "Project has been successfully created.", ActualAlertMessage2);
        }

        [Test, Order(2)]
        [TestCase(TestName = "Test 2 - Verify that user can successfully search created Module Project")]
        public void Search_Module_Project()
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("https://", "https://" + util.GetCredentials("Account 1", "Username") + ":" + util.GetCredentials("Account 1", "Password") + "@") + "SiteAssets/Pages/ProjectHome.aspx");
            Thread.Sleep(15000);
            action.UserSearchForProjectRequestViaSearchTextbox(driver, ProjName);
            string PDP_ProjName = util.GetText(driver, By.CssSelector("span[id='spanPrjName']"));
            test.validateStringIsCorrect(driver, ProjName.ToUpper(), PDP_ProjName);
            ProjUrl = driver.Url;
            Console.WriteLine(ProjUrl);
        }

        [Test, Order(3)]
        [TestCase(TestName = "Test 3 - Verify that the following section is present in PDP - Team Site, Documention, Business Case, Generic Information, Milestone Information, Phase Gate")]
        public void PDPSection_Module_Project()
        {
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            //Project Information
            test.validateElementIsPresent(driver, By.CssSelector("div[id='divViewProject']"));
            //Sponsoring Market
            test.validateElementIsPresent(driver, By.CssSelector("div[id='divViewSBSAllocation']"));
            //Collaboration Links
            test.validateElementIsPresent(driver, By.CssSelector("div[id='collabSectionDiv']"));
            //Team Site
            test.validateElementIsPresent(driver, By.CssSelector("div[id='teamSectionDiv']"));
            //Document Section
            test.validateElementIsPresent(driver, By.CssSelector("div[id='docSectionDiv']"));
            //Business case
            test.validateElementIsPresent(driver, By.CssSelector("div[id='pdBusinessCaseDiv']"));
            //Generic Information
            test.validateElementIsPresent(driver, By.CssSelector("div[id='genericInfoDiv']"));
            //Phase Gate Approval
            test.validateElementIsPresent(driver, By.CssSelector("div[id='phaseGateInfoDiv']"));

        }

        //Test that Initiate VIR Button is disabled by Default (no proj/technical mgr yet and security assessment is not yet determined

        [Test, Order(4)]
        [TestCase(TestName = "Test 4 - Verify Phase Gate Initial Status for standard project (no sec assesment and selected proj/technical manager)")]
        public void PhaseGate_Initial_Status_Module()
        {
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            test.validateStringIsCorrect(driver, "PSD1", util.GetText(driver, By.CssSelector("span[data-bind='text: data.nextMilestone']")));
            test.validateElementIsPresent(driver, By.CssSelector("span[id='psd0Completed']"));
            test.validateStringIsCorrect(driver, "Initiated on " + DateToday + " by InAppsTest20", util.GetText(driver, By.CssSelector("ul[id='panelbarPSD0'] * div[class='col-md-6']")));
            test.validateStringIsCorrect(driver, "Preliminary Security Assessment needs to be completed.", util.GetText(driver, By.CssSelector("label[id='sesInvalidErrorMsg']")));
            test.validateStringIsCorrect(driver, "Project Manager/Lead and/or Technical Lead is missing in the", util.GetText(driver, By.CssSelector("label[id='initiateWFLabelForTeamList']")));
            test.validateElementIsNotEnabled(driver, By.CssSelector("button[id='btn_initiateWF']"));
            action.AddProjectTechnicalLead(driver, "Sorosoro", "Durana");
        }

        [Test, Order(5)]
        [TestCase(TestName = "Test 5 - Verify that user can save an update to Business Case Project value")]
        public void BC_Proj_Value_Module_Project()
        {
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            //Project Information
            action.IClick(driver, By.CssSelector("div[id='headingOnebc'] span[class='fas fa-pencil-alt pull-right']"));
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            Thread.Sleep(10000);
            if (util.CheckElement(driver, By.CssSelector("div[id='divViewProject']"), 5) == false || util.CheckElement(driver, By.CssSelector("div[id='divEditProject']"), 5) == false)
            {
                driver.Navigate().Refresh();
            }
            action.SetSecurityExposureToZero(driver);
            driver.SwitchTo().Window(driver.WindowHandles.First());
            driver.Navigate().Refresh();
            Thread.Sleep(15000);
            action.IClick(driver, By.CssSelector("div[id='headingOnebc'] a"));
            test.validateStringIsCorrect(driver, "0", util.GetText(driver, By.CssSelector("div[id='collapseOnebc'] span[data-bind='text:psaScore']")));
        }

        [Test, Order(6)]
        [TestCase(TestName = "Test 6 - Verify that user can save an update to Business Case Investment")]
        public void BC_Investment_Module_Project()
        {
            string RandomNumber = util.RandomNumber();
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            //Project Information
            action.IClick(driver, By.CssSelector("div[id='headingThreebc'] a>span[class='fas fa-pencil-alt pull-right']"));
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            Thread.Sleep(10000);
            action.EditInvestmentBC(driver, RandomNumber);
            driver.SwitchTo().Window(driver.WindowHandles.First());
            driver.Navigate().Refresh();
            Thread.Sleep(15000);
            action.IClick(driver, By.CssSelector("div[id='headingThreebc'] a"));
            test.validateStringIsCorrect(driver, RandomNumber, util.GetText(driver, By.CssSelector("span[data-bind='text: labor ']")).Split('.')[0]);
            string TotalDevCost = util.GetText(driver, By.CssSelector("span[data-bind='text: totalNonBurdenCost']"));
            action.IClick(driver, By.CssSelector("div[id='headingsixbc'] a"));
            test.validateStringIsCorrect(driver, TotalDevCost + "0", util.GetText(driver, By.CssSelector("span[data-bind='text: projectFinancialData.directDevCost']")));
        }

        [Test, Order(7)]
        [TestCase("Generic submitted. System will take some time for new generic site provision", TestName = "Test 7 - Verify that user can successfully create Generics")]
        public void CreateGenerics_Module_Project(string ExpectedSuccessMessage)
        {
            GenericName = DateToday + "_" + util.RandomNumber();
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            action.CreateGenerics(driver, "streamlined", GenericName);
            test.validateStringIsCorrect(driver, ExpectedSuccessMessage, util.GetAlertMessageThenAccept(driver));
            test.validateStringInstance(driver, util.GetText(driver, By.CssSelector("div[id='genericgrid'] * tbody>tr:last-child * span[data-bind='html:GenericName']")), GenericName);

        }

        [Test, Order(8)]
        [TestCase(TestName = "Test 8 - Verify Generic PTC and Milestone Initial Status")]
        public void PTCPhase1Status_Standard_Project()
        {
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            action.IClick(driver, By.CssSelector("div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable'] * tbody>tr * a[class='k-icon k-i-expand']"));
            /*****Verify Approval Plan Status*****/
            test.validateStringIsCorrect(driver, "Pending", util.GetText(driver, By.CssSelector("div[class='milecomponentWraperForModule'] span[data-bind*='text:GenericApprovalStatusForModule']")));
            /*****Verify Approval Plan Status*****/
            test.validateStringIsCorrect(driver, "N/A", util.GetText(driver, By.CssSelector("span[data-bind*='text:LCDRStatusForModule']")));
            /*****Verify RTS Status*****/
            test.validateStringIsCorrect(driver, "Pending", util.GetText(driver, By.CssSelector("div[class='milecomponentWraperForModule'] span[data-bind*='text:RTSStatus']")));

            test.validateElementIsPresent(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(1) * span[class='initiateReady_ptc']"));
            test.validateElementIsPresent(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(2) * span[class='notStarted_ptc']"));
            test.validateElementIsPresent(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(3) * span[class='notStarted_ptc']"));
            test.validateElementIsPresent(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(4) * span[class='notStarted_ptc']"));
        }

        [Test, Order(9)]
        [TestCase(TestName = "Test 9 - Verify User can download and save checklist")]
        public void DownloadAndSaveChecklist()
        {
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            action.IClick(driver, By.CssSelector("div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable'] * tbody>tr * a[class='k-icon k-i-expand']"));
            action.IClick(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(1)>span"));
            action.DownloadAndSaveChecklist(driver);
            test.validateElementIsPresent(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(1) span[class='inProgress_ptc']"));
            test.validateElementIsNotEnabled(driver, By.CssSelector("button[data-bind*='click: saveCheckList']"));
        }

        [Test, Order(10)]
        [TestCase(TestName = "Test 10 - Verify User can complete task in PTC Generics - PSD1")]
        public void Approved_Generic_TaskList_PSD1()
        {
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            action.IClick(driver, By.CssSelector("div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable'] * tbody>tr * a[class='k-icon k-i-expand']"));
            int Taskcount = util.GetElementCount(driver, By.CssSelector("table[id='psd100Grid']>tbody>tr"));
            for (int x = 1; util.CheckElement(driver, By.CssSelector("table[id='psd100Grid']>tbody>tr:nth-child(" + x + ")"), 5); x++)
            {
                if (util.CheckElement(driver, By.CssSelector("table[id='psd100Grid']>tbody>tr:nth-child(" + x + ") * span[class='ptc_pending']"), 5))
                {
                    if (util.CheckElement(driver, By.CssSelector("table[id='psd100Grid']>tbody>tr:nth-child(" + x + ")>td>button"), 5))
                    {
                        action.IClick(driver, By.CssSelector("table[id='psd100Grid']>tbody>tr:nth-child(" + x + ")>td>button"));
                        Thread.Sleep(2000);
                        action.ProcessGtM(driver, "Sorosoro, Aries");
                    }

                    action.IClick(driver, By.CssSelector("table[id='psd100Grid']>tbody>tr:nth-child(" + x + ") * span[class='ptc_pending']"));
                    action.SignOff_Task(driver, "Test Only", "http://www.analog.com");
                    if (x == Taskcount)
                    {
                        driver.Navigate().Refresh();
                        Thread.Sleep(15000);
                        action.IClick(driver, By.CssSelector("div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable'] * tbody>tr * a[class='k-icon k-i-expand']"));
                        action.IClick(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(1)>span"));

                    }
                    test.validateElementIsPresent(driver, By.CssSelector("table[id='psd100Grid']>tbody>tr:nth-child(" + x + ") * span[class^='signedoff_ptc']"));
                }
            }
            test.validateElementIsPresent(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(1) span[class='completed_ptc customSelected']"));
            test.validateStringIsCorrect(driver, DateToday, util.GetText(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(1) span[data-bind*='Completeddate']")));
        }

        [Test, Order(11)]
        [TestCase(TestName = "Test 11 - Verify User can complete task in PTC Generics - PSD2")]
        public void Approved_Generic_TaskList_PSD2()
        {
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            action.IClick(driver, By.CssSelector("div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable'] * tbody>tr * a[class='k-icon k-i-expand']"));
            action.IClick(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(2)>span"));
            action.DownloadAndSaveChecklist(driver);
            int Taskcount = util.GetElementCount(driver, By.CssSelector("table[id='psd200Grid']>tbody>tr"));
            for (int x = 1; util.CheckElement(driver, By.CssSelector("table[id='psd200Grid']>tbody>tr:nth-child(" + x + ")"), 5); x++)
            {
                if (util.CheckElement(driver, By.CssSelector("table[id='psd200Grid']>tbody>tr:nth-child(" + x + ") * span[class='ptc_pending']"), 5))
                {
                    if (util.CheckElement(driver, By.CssSelector("table[id='psd200Grid']>tbody>tr:nth-child(" + x + ")>td>button"), 5))
                    {
                        action.IClick(driver, By.CssSelector("table[id='psd200Grid']>tbody>tr:nth-child(" + x + ")>td>button"));
                        Thread.Sleep(2000);
                        action.ProcessGtM(driver, "Sorosoro, Aries");
                    }

                    action.IClick(driver, By.CssSelector("table[id='psd200Grid']>tbody>tr:nth-child(" + x + ") * span[class='ptc_pending']"));
                    action.SignOff_Task(driver, "Test Only", "http://www.analog.com");
                    if (x == Taskcount)
                    {
                        driver.Navigate().Refresh();
                        Thread.Sleep(15000);
                        action.IClick(driver, By.CssSelector("div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable'] * tbody>tr * a[class='k-icon k-i-expand']"));
                        action.IClick(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(2)>span"));
                    }
                    test.validateElementIsPresent(driver, By.CssSelector("table[id='psd200Grid']>tbody>tr:nth-child(" + x + ") * span[class^='signedoff_ptc']"));
                }
            }
            test.validateElementIsPresent(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(2) span[class='completed_ptc customSelected']"));
            test.validateStringIsCorrect(driver, DateToday, util.GetText(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(2) span[data-bind*='Completeddate']")));
        }

        [Test, Order(12)]
        [TestCase(TestName = "Test 12 - Verify user can add and complete task in PTC Generics - PDS2x")]
        public void Approved_Generic_TaskList_PSD2x()
        {
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            action.IClick(driver, By.CssSelector("div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable'] * tbody>tr * a[class='k-icon k-i-expand']"));

        }

        [Test, Order(13)]
        [TestCase(TestName = "Test 13 - Verify User can complete task in PTC Generics - PSD3")]
        public void Approved_Generic_TaskList_PSD3()
        {
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            action.IClick(driver, By.CssSelector("div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable'] * tbody>tr * a[class='k-icon k-i-expand']"));
            action.IClick(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(3)>span"));
            action.DownloadAndSaveChecklist(driver);
            driver.Navigate().Refresh();
            Thread.Sleep(15000);
            action.IClick(driver, By.CssSelector("div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable'] * tbody>tr * a[class='k-icon k-i-expand']"));
            int Taskcount = util.GetElementCount(driver, By.CssSelector("table[id='psd300Grid']>tbody>tr"));
            for (int x = 1; util.CheckElement(driver, By.CssSelector("table[id='psd300Grid']>tbody>tr:nth-child(" + x + ")"), 5); x++)
            {
                if (util.CheckElement(driver, By.CssSelector("table[id='psd300Grid']>tbody>tr:nth-child(" + x + ") * span[class='ptc_pending']"), 5))
                {
                    if (util.CheckElement(driver, By.CssSelector("table[id='psd300Grid']>tbody>tr:nth-child(" + x + ")>td>button"), 5))
                    {
                        action.IClick(driver, By.CssSelector("table[id='psd300Grid']>tbody>tr:nth-child(" + x + ")>td>button"));
                        Thread.Sleep(2000);
                        action.ProcessGtM(driver, "Sorosoro, Aries");
                    }

                    action.IClick(driver, By.CssSelector("table[id='psd300Grid']>tbody>tr:nth-child(" + x + ") * span[class='ptc_pending']"));
                    action.SignOff_Task(driver, "Test Only", "http://www.analog.com");
                    if (x == Taskcount)
                    {
                        driver.Navigate().Refresh();
                        Thread.Sleep(15000);
                        action.IClick(driver, By.CssSelector("div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable'] * tbody>tr * a[class='k-icon k-i-expand']"));
                        action.IClick(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(3)>span"));

                        //    test.validateStringIsCorrect(driver, "PSD4 Launch PTC Phase will be auto downloaded on sign off of this last task", util.GetAlertMessageThenAccept(driver));
                        //    Thread.Sleep(15000);
                        //    action.IClick(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(3)>span"));
                    }
                    test.validateElementIsPresent(driver, By.CssSelector("table[id='psd300Grid']>tbody>tr:nth-child(" + x + ") * span[class^='signedoff_ptc']"));
                }
            }
            test.validateElementIsPresent(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(3) span[class='completed_ptc customSelected']"));
            test.validateStringIsCorrect(driver, DateToday, util.GetText(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(3) span[data-bind*='Completeddate']")));

        }

        [Test, Order(14)]
        [TestCase(TestName = "Test 14 - Verify User can complete task in PTC Generics - PSD4")]
        public void Approved_Generic_TaskList_PSD4()
        {
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            action.IClick(driver, By.CssSelector("div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable'] * tbody>tr * a[class='k-icon k-i-expand']"));
            action.IClick(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(4)>span"));
            action.DownloadAndSaveChecklist(driver);
            driver.Navigate().Refresh();
            Thread.Sleep(15000);
            action.IClick(driver, By.CssSelector("div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable'] * tbody>tr * a[class='k-icon k-i-expand']"));
            int Taskcount = util.GetElementCount(driver, By.CssSelector("table[id='psd400Grid']>tbody>tr"));
            for (int x = 1; util.CheckElement(driver, By.CssSelector("table[id='psd400Grid']>tbody>tr:nth-child(" + x + ")"), 5); x++)
            {
                if (util.CheckElement(driver, By.CssSelector("table[id='psd400Grid']>tbody>tr:nth-child(" + x + ") * span[class='ptc_pending']"), 5))
                {
                    if (util.CheckElement(driver, By.CssSelector("table[id='psd400Grid']>tbody>tr:nth-child(" + x + ")>td>button"), 5))
                    {
                        action.IClick(driver, By.CssSelector("table[id='psd400Grid']>tbody>tr:nth-child(" + x + ")>td>button"));
                        Thread.Sleep(2000);
                        action.ProcessGtM(driver, "Sorosoro, Aries");
                    }

                    action.IClick(driver, By.CssSelector("table[id='psd400Grid']>tbody>tr:nth-child(" + x + ") * span[class='ptc_pending']"));
                    action.SignOff_Task(driver, "Test Only", "http://www.analog.com");
                    test.validateElementIsPresent(driver, By.CssSelector("table[id='psd400Grid']>tbody>tr:nth-child(" + x + ") * span[class^='signedoff_ptc']"));
                }
            }
            test.validateElementIsPresent(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(4) span[class='completed_ptc']"));
            test.validateStringIsCorrect(driver, DateToday, util.GetText(driver, By.CssSelector("ul[id='panelbarPTC']>li:nth-child(4) span[data-bind*='Completeddate']")));

        }

        //[Test, Order(14)]
        //[TestCase(TestName = "Test 15 - Verify User can initiate and complete Phasegate VIR Workflow")]
        //public void Approved_VIR_PG()
        //{
        //    action.Navigate(driver, ProjUrl);
        //    Thread.Sleep(15000);
        //    test.validateElementIsEnabled(driver, By.CssSelector("button[id='btn_initiateWF']"));
        //    test.validateStringIsCorrect(driver, "Initiate Value and Impact Review Workflow", util.GetText(driver, By.CssSelector("button[id='btn_initiateWF']")));
        //    action.Initiate_PhaseGate(driver, "This is just a Test");
        //    test.validateStringIsCorrect(driver, "You have successfully initiated this phase.", util.GetAlertMessageThenAccept(driver));
        //    Thread.Sleep(10000);
        //    test.validateStringIsCorrect(driver, "Initiated on " + DateToday + " by Sorosoro, Aries", util.GetText(driver, By.CssSelector("div[id='rowValue and Impact Review'] div[class='col-md-6']")));
        //    test.validateStringIsCorrect(driver, "Global Ops Rep", util.GetText(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(1)>div[id='app0ApproverData']>table>tbody>tr>td:nth-child(2)>span")));

        //    int Approver_Count = util.GetElementCount(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(1)>div[id='app0ApproverData']>table>tbody>tr"));
        //    for (int x = 1; util.CheckElement(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(1)>div[id='app0ApproverData']>table>tbody>tr:nth-child(" + x + ")"), 5); x++)
        //    {
        //        action.IType(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(1)>div[id='app0ApproverData']>table>tbody>tr:nth-child(" + x + ") * textarea"), "This is just a Test");
        //        action.IClick(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(1)>div[id='app0ApproverData']>table>tbody>tr:nth-child(" + x + ") * button[id*='approver']"));
        //        Thread.Sleep(5000);
        //        test.validateStringIsCorrect(driver, "You have successfully approved this phase.", util.GetAlertMessageThenAccept(driver));
        //        Thread.Sleep(10000);
        //        if (x == Approver_Count)
        //        {
        //            test.validateStringIsCorrect(driver, "Approved on " + DateToday, util.GetText(driver, By.CssSelector("div[id='rowValue and Impact Review'] div[class='col-md-6']>span[id='spn_ApprovedBy']")));
        //        }
        //    }
        //}

        [Test, Order(15)]
        [TestCase(TestName = "Test 15 - Verify User can initiate and complete Phase gate PSD1 Workflow")]
        public void Approved_PDS1()
        {
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            test.validateElementIsEnabled(driver, By.CssSelector("button[id='btn_initiateWF']"));
            test.validateStringIsCorrect(driver, "Initiate PSD1 Workflow", util.GetText(driver, By.CssSelector("button[id='btn_initiateWF']")));
            action.Initiate_PhaseGate(driver, "This is just a Test");
            test.validateStringIsCorrect(driver, "You have successfully initiated this phase.", util.GetAlertMessageThenAccept(driver));
            Thread.Sleep(10000);
            test.validateStringIsCorrect(driver, "Initiated on " + DateToday + " by Sorosoro, Aries", util.GetText(driver, By.CssSelector("div[id='rowPSD1'] div[class='col-md-6']")));
            //action.IClick(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(1)>div>div"));

            int Approver_Count = util.GetElementCount(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(1)>div[id='app0ApproverData']>table>tbody>tr"));
            for (int x = 1; util.CheckElement(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(1)>div[id='app0ApproverData']>table>tbody>tr:nth-child(" + x + ")"), 5); x++)
            {
                action.IType(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(1)>div[id='app0ApproverData']>table>tbody>tr:nth-child(" + x + ") * textarea"), "This is just a Test");
                action.IClick(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(1)>div[id='app0ApproverData']>table>tbody>tr:nth-child(" + x + ") * button[id*='approver']"));
                Thread.Sleep(5000);
                test.validateStringIsCorrect(driver, "You have successfully approved this phase.", util.GetAlertMessageThenAccept(driver));
                Thread.Sleep(10000);
                if (x == Approver_Count)
                {
                    test.validateStringIsCorrect(driver, "Approved on " + DateToday, util.GetText(driver, By.CssSelector("div[id='rowPSD1'] div[class='col-md-6']>span[id='spn_ApprovedBy']")));
                }
                //action.IClick(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(1)>div>div"));
            }
        }

        [Test, Order(16)]
        [TestCase(TestName = "Test 16 - Verify User can initiate and complete Phase gate PSD2 Workflow")]
        public void Approved_PDS2()
        {
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            test.validateElementIsEnabled(driver, By.CssSelector("button[id='btn_initiateWF']"));
            test.validateStringIsCorrect(driver, "Initiate PSD2 Workflow", util.GetText(driver, By.CssSelector("button[id='btn_initiateWF']")));
            action.Initiate_PhaseGate(driver, "This is just a Test");
            test.validateStringIsCorrect(driver, "You have successfully initiated this phase.", util.GetAlertMessageThenAccept(driver));
            Thread.Sleep(10000);
            test.validateStringIsCorrect(driver, "Initiated on " + DateToday + " by Sorosoro, Aries", util.GetText(driver, By.CssSelector("div[id='rowPSD2'] div[class='col-md-6']")));
            //action.IClick(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(2)>div>div"));
            //test.validateStringIsCorrect(driver, "Global Ops Rep", util.GetText(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(3)>div[id='app0ApproverData']>table>tbody>tr>td:nth-child(2)>span")));

            int Approver_Count = util.GetElementCount(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(2)>div[id='app0ApproverData']>table>tbody>tr"));
            for (int x = 1; util.CheckElement(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(2)>div[id='app0ApproverData']>table>tbody>tr:nth-child(" + x + ")"), 5); x++)
            {
                action.IType(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(2)>div[id='app0ApproverData']>table>tbody>tr:nth-child(" + x + ") * textarea"), "This is just a Test");
                action.IClick(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(2)>div[id='app0ApproverData']>table>tbody>tr:nth-child(" + x + ") * button[id*='approver']"));
                Thread.Sleep(5000);
                test.validateStringIsCorrect(driver, "You have successfully approved this phase.", util.GetAlertMessageThenAccept(driver));
                Thread.Sleep(10000);
                if (x == Approver_Count)
                {
                    test.validateStringIsCorrect(driver, "Approved on " + DateToday, util.GetText(driver, By.CssSelector("div[id='rowPSD2'] div[class='col-md-6']>span[id='spn_ApprovedBy']")));
                }
                //action.IClick(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(2)>div>div"));
            }
        }

        [Test, Order(17)]
        [TestCase(TestName = "Test 17 - Verify User can initiate and complete Phase gate PSD3 Workflow")]
        public void Approved_PDS3()
        {
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            test.validateElementIsEnabled(driver, By.CssSelector("button[id='btn_initiateWF']"));
            test.validateStringIsCorrect(driver, "Initiate PSD3 Workflow", util.GetText(driver, By.CssSelector("button[id='btn_initiateWF']")));
            action.Initiate_PhaseGate(driver, "This is just a Test");
            test.validateStringIsCorrect(driver, "You have successfully initiated this phase.", util.GetAlertMessageThenAccept(driver));
            Thread.Sleep(10000);
            test.validateStringIsCorrect(driver, "Initiated on " + DateToday + " by Sorosoro, Aries", util.GetText(driver, By.CssSelector("div[id='rowPSD3'] div[class='col-md-6']")));
            //action.IClick(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(3)>div>div"));
            //test.validateStringIsCorrect(driver, "Global Ops Rep", util.GetText(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(3)>div[id='app0ApproverData']>table>tbody>tr>td:nth-child(2)>span")));

            int Approver_Count = util.GetElementCount(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(3)>div[id='app0ApproverData']>table>tbody>tr"));
            for (int x = 1; util.CheckElement(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(3)>div[id='app0ApproverData']>table>tbody>tr:nth-child(" + x + ")"), 5); x++)
            {
                action.IType(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(3)>div[id='app0ApproverData']>table>tbody>tr:nth-child(" + x + ") * textarea"), "This is just a Test");
                action.IClick(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(3)>div[id='app0ApproverData']>table>tbody>tr:nth-child(" + x + ") * button[id*='approver']"));
                Thread.Sleep(5000);
                test.validateStringIsCorrect(driver, "You have successfully approved this phase.", util.GetAlertMessageThenAccept(driver));
                Thread.Sleep(10000);
                if (x == Approver_Count)
                {
                    test.validateStringIsCorrect(driver, "Approved on " + DateToday, util.GetText(driver, By.CssSelector("div[id='rowPSD3'] div[class='col-md-6']>span[id='spn_ApprovedBy']")));
                }
                //action.IClick(driver, By.CssSelector("ul[id='psppanelbar']>li:nth-child(3)>div>div"));
            }
            test.validateStringIsCorrect(driver, "The system will automatically set the Project to complete 90 days after all Generics have released and Launch PTCs have been completed.", util.GetText(driver, By.CssSelector("label[id='initiateWFLabel']")));
        }

        [Test, Order(18)]
        [TestCase(TestName = "Test 18 - Verify Plan Approval behaviors after PTC and Phase Gate Approval/Sign Off")]
        public void PA_CC_Verification()
        {
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            action.IClick(driver, By.CssSelector("div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable'] * tbody>tr * a[class='k-icon k-i-expand']"));
            test.validateStringIsCorrect(driver, DateToday, util.GetText(driver, By.CssSelector("div[class='milecomponentWraperForModule'] span[id='span1PSD1']")));
            test.validateStringIsCorrect(driver, DateToday, util.GetText(driver, By.CssSelector("div[class='milecomponentWraperForModule'] span[id='span1Current Commit']")));
            test.validateStringIsCorrect(driver, "Completed", util.GetText(driver, By.CssSelector("div[class='milecomponentWraperForModule'] span[data-bind*='text:GenericApprovalStatusForModule']")));
        }

        [Test, Order(19)]
        [TestCase(TestName = "Test 19 - Verify Layout & Critical Design Review milestone after PTC and Phase Gate Approval/Sign Off")]
        public void LCDR_Verification()
        {
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            action.IClick(driver, By.CssSelector("div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable'] * tbody>tr * a[class='k-icon k-i-expand']"));
            action.IClick(driver, By.CssSelector("span[id='editMilestoneSecForModule']"));
            Thread.Sleep(2000);
            action.IType(driver, By.CssSelector("div[class='milecomponentWraperForModule'] input[data-bind*='value:LCDRForModule , visible:  date3']"), DateToday);
            action.IClick(driver, By.CssSelector("div[class='milecomponentWraperForModule'] input[id='Tapeout']"));
            action.IClick(driver, By.CssSelector("div[class='milecomponentWraperForModule'] button[data-bind*='click: savemilestoneinfo']"));
            test.validateStringIsCorrect(driver, "Milestone details saved successfully", util.GetAlertMessageThenAccept(driver));
            Thread.Sleep(5000);
            test.validateStringIsCorrect(driver, "Completed", util.GetText(driver, By.CssSelector("span[data-bind*='text:LCDRStatusForModule']")));
        }

        [Test, Order(20)]
        [TestCase(TestName = "Test 20 - Verify RTS milestone after PTC and Phase Gate Approval/Sign Off")]
        public void RTS_Verification()
        {
            action.Navigate(driver, ProjUrl);
            Thread.Sleep(15000);
            action.IClick(driver, By.CssSelector("div[id='genericgrid']>div[class='k-grid-content k-auto-scrollable'] * tbody>tr * a[class='k-icon k-i-expand']"));
            action.IClick(driver, By.CssSelector("span[id='editMilestoneSecForModule']"));
            action.IClick(driver, By.CssSelector("div[class='milecomponentWraperForModule'] input[id='RTS']"));
            action.IClick(driver, By.CssSelector("div[class='milecomponentWraperForModule'] button[data-bind*='click: savemilestoneinfo']"));
            test.validateStringIsCorrect(driver, "Milestone details saved successfully", util.GetAlertMessageThenAccept(driver));
            Thread.Sleep(5000);
            test.validateStringIsCorrect(driver, "Completed", util.GetText(driver, By.CssSelector("div[class='milecomponentWraperForModule'] span[data-bind*='text:RTSStatus']")));
        }

    }
}

